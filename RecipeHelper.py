import yaml
import numpy as np
import pandas as pd
import json
import os
import pickle
import re

############################# Function checking if 5-bit signed value is in a proper range ##############################

def check_5bit_value(dec_value):
    if dec_value > 31 or dec_value < -32:
        raise ValueError(f"Value: {dec_value} out of range [-32,31]")

######################################### Function converting decimals to 2's complements ###############################


def int_to_twoc(dec_value):
    check_5bit_value(dec_value)
    bin_value = f"{abs(dec_value):05b}"
    if dec_value >= 0:
        twoc_value = 3*"0" + bin_value
    else:
        bin_value = "0" + bin_value
        mask = 0b111111
        neg_value = f"{(int(bin_value,2)^mask):06b}"
        twoc_value = f"{(int(neg_value,2)+1):06b}"
        twoc_value = 2*"0" + twoc_value
    return twoc_value
