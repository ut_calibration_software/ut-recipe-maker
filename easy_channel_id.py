import re
import yaml

strip_mask = 0x1ff
sector_mask = 0x200
module_mask = 0x1c00
face_mask = 0x2000
stave_mask = 0x3c000
layer_mask = 0xc0000
side_mask = 0x100000
det_type_mask = 0x600000
det_type_ut = 2
max_strip_number = 511
max_sector_number = 1
max_module_number = 7
max_face_number = 1
max_stave_number = 8
max_layer_number = 3
max_side_number = 1
side_names = ("C", "A")
layer_names = ("aX", "aU", "bV", "bX")


def get_range_channel_id():
    with open("create_readoutmap/online_name_to_chanid_map.yaml") as f:
        map = yaml.safe_load(f)
    channelids = []
    for item in map:
        channelids.append(map[item])

    channelids.sort()
    print("Maximum ChannelID =", channelids[-1], "or",
          get_online_name(channelids[-1]), "or", get_nickname(channelids[-1]))
    print("Minimum ChannelID =", channelids[0], "or",
          get_online_name(channelids[0]), "or", get_nickname(channelids[0]))


def extract_bits(aChan):
    strip = (int(bin(aChan)[2:], 2) & int(bin(strip_mask)[2:], 2))
    sector = (int(bin(aChan)[2:], 2) & int(bin(sector_mask)[2:], 2)) >> 9
    module = (int(bin(aChan)[2:], 2) & int(bin(module_mask)[2:], 2)) >> 10
    face = (int(bin(aChan)[2:], 2) & int(bin(face_mask)[2:], 2)) >> 13
    stave = (int(bin(aChan)[2:], 2) & int(bin(stave_mask)[2:], 2)) >> 14
    layer = (int(bin(aChan)[2:], 2) & int(bin(layer_mask)[2:], 2)) >> 18
    side = (int(bin(aChan)[2:], 2) & int(bin(side_mask)[2:], 2)) >> 20
    det_type = (int(bin(aChan)[2:], 2) & int(bin(det_type_mask)[2:], 2)) >> 21
    return det_type, side, layer, stave, face, module, sector, strip


def make_channel_id(side,
                    layer,
                    stave,
                    face,
                    module,
                    sector,
                    det_type=2,
                    strip=0):
    return (int(det_type) << 21) + (side << 20) + (layer << 18) + (
        stave << 14) + (face << 13) + (module << 10) + (sector << 9) + strip


def get_online_name_from_nickname(nickname):
    return get_online_name(get_channel_id_from_nickname(nickname))


def get_channel_id_from_nickname(nickname):
    pattern = r"UT([AC])sideUT(\w{2})Stave(\d{1})Face(\d{1})Module(\d{1})Sector(\d{1})"
    match = re.search(pattern, nickname)
    side = side_names.index(match.group(1))
    layer = layer_names.index(match.group(2))
    stave = int(match.group(3))
    face = int(match.group(4))
    module = int(match.group(5))
    sector = int(match.group(6))
    return make_channel_id(side, layer, stave, face, module, sector)


def get_channel_id_from_online_name(online_name):
    with open("create_readoutmap/online_name_to_chanid_map.yaml") as f:
        map = yaml.safe_load(f)
    return map[online_name]


def get_nickname(aChan):
    det_type, side, layer, stave, face, module, sector, _ = extract_bits(aChan)
    assert det_type == det_type_ut
    nickname = f"UT{side_names[side]}sideUT{layer_names[layer]}Stave{stave}Face{face}Module{module}Sector{sector}"
    return nickname


def get_online_name(aChan):
    det_type, side, layer, stave, face, module, sector, _ = extract_bits(aChan)
    assert det_type == det_type_ut
    side_str = side_names[side]
    layer_str = layer_names[layer]
    stave_str = stave + 1

    # module_str
    if stave == 0:
        if face == 0:
            if module < 4:
                module_str = "B_S" + str(4 - module)
            else:
                module_str = "T_M" + str(module - 3)
        if face == 1:
            if module < 4:
                module_str = "B_M" + str(4 - module)
            else:
                module_str = "T_S" + str(module - 3)
    else:
        if face == 0:
            if module < 4:
                module_str = "B_S" + str(3 - module)
            else:
                module_str = "T_M" + str(module - 3)
        elif face == 1:
            if module < 4:
                module_str = "B_M" + str(4 - module)
            else:
                module_str = "T_S" + str(module - 4)

    # sector_str
    if stave < 2 and face == 0 and module >= 2 and module < 5:
        sector_str = "W" if sector == 0 else "E"
    elif stave < 2 and face == 1 and module > 2 and module <= 5:
        sector_str = "W" if sector == 0 else "E"
    else:
        assert sector == 0
        sector_str = ""

    name = f"UT{layer_str}_{stave_str}{side_str}{module_str}{sector_str}"
    return name