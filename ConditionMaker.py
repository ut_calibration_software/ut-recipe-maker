"""
ConditionMaker.py

Created on: July, 2024
Author: Karol Sowa (karol.mateusz.sowa@cern.ch)

"""

from RecipeHelper import *
from easy_channel_id import *

############################################# Filepath parameters #############################################

RUN_NUMBER = 306326
INPUT_DIR= f"/data1/DATA_SOWA/LocalCondDB/NZS_data/{RUN_NUMBER}/"


PEDESTAL_FILE = INPUT_DIR + "pedestals.csv"
CMS_NOISE_FILE = INPUT_DIR + "CMS_noise.csv"
CM_FILE = INPUT_DIR + "commonMode.csv"
MASKING_FILE = INPUT_DIR + "masks_cfg.txt" 

REGISTER_DUMPS =  [INPUT_DIR + "register_dump_A_side.txt", INPUT_DIR + "register_dump_C_side.txt"]

OUTPUT_PEDESTALS = f'PedestalValues_{RUN_NUMBER}.yml'
OUTPUT_NOISE = f'NoiseValues_{RUN_NUMBER}.yml'
OUTPUT_THRESHOLDS = f'Thresholds_{RUN_NUMBER}.yml'
OUTPUT_ZS_CHANNELS = f'HitLimits_{RUN_NUMBER}.yml'
OUTPUT_READOUT = f'ReadoutSectors_{RUN_NUMBER}.yml'

FORMATTING_PEDESTALS = True # Beautiful pedestal formatting

############################################# Default values ##################################################

DEFAULT_PEDESTAL_VALUE = 31
DEFAULT_CMS_NOISE_VALUE = -1
DEFAULT_CM_VALUE = -1
DEFAULT_GAIN_VALUE = 2250.0
DEFAULT_THRESHOLD_VALUE = -1
DEFAULT_CHANNELS_VALUE = 63
DEFAULT_STATUS_VALUE = 1

############################################# Loading translator files ########################################

ECS_TO_CHID_PATH = "./online_name_to_chanid_map.yaml"
CHID_TO_ECS_PATH = "./translator.pkl"

with open(ECS_TO_CHID_PATH, 'r') as file:
    try:
        ecs_sectors = yaml.safe_load(file)
    except yaml.YAMLError as exc:
        print(exc)
        
with open(CHID_TO_ECS_PATH, "rb") as handle:
    tr_pkl = pickle.load(handle)
        
##################################### Redefinition of the Dumper class #########################################

class MyDumper(yaml.Dumper):

    def increase_indent(self, flow = False, indentless=False):
        self.indents.append(self.indent)
        if self.indent is None:
            if flow:
                self.indent = self.best_indent
            else:
                self.indent = 0
        elif not indentless:
            if flow:
                self.indent = self.column    #Aligns the indent of the arrays
            else:
                self.indent += self.best_indent
        
################################## Filling out the YAML condition files ########################################

print("[INFO] Gathering info about the pedestals...")

###--------------------------------------- PEDESTALS---------------------------------------------###
pedestal_dict = {'PedestalValues': {}}
pedestal_dict['PedestalValues'] = {get_nickname(aChan): {
                        'Pedestals': 512*[DEFAULT_PEDESTAL_VALUE],
                        'SectorPedestal': [0,0,0,0,0]
                       } for aChan in ecs_sectors.values()}


convertfunc1 = lambda x: round(float(x))
pedestal_df = pd.read_csv(PEDESTAL_FILE, converters = {1: convertfunc1}, header = None, usecols = [0,1], names = ["ChannelID", "pedestal"], index_col = "ChannelID")
channels = pedestal_df.index.to_list()
pedestals = pedestal_df["pedestal"].to_list()

for channelID, pedestal in zip(channels, pedestals):
    nickname = get_nickname(channelID)
    index = channelID - get_channel_id_from_nickname(nickname)
    pedestal_dict['PedestalValues'][nickname]['Pedestals'][index] = pedestal


with open(OUTPUT_PEDESTALS, 'w') as outfile:
    yaml.dump(pedestal_dict, outfile, Dumper=MyDumper, default_flow_style=None, explicit_start=True)
    
    
### PEDESTAL FORMATTING
    

if FORMATTING_PEDESTALS == True:
    TEMP_PEDESTAL = open(OUTPUT_PEDESTALS, 'r')
    LINES = TEMP_PEDESTAL.readlines()
    FINAL_PEDESTAL = open(OUTPUT_PEDESTALS, 'w')
    val_ctr = 0
    strip_ctr = 0
    for line in LINES:
        if "," not in line and "]" not in line:
            FINAL_PEDESTAL.write(line)
        elif "Sector" in line:
            newline = ""
            
            prefix = line.split("[")[0] + "["
            line = line.split("[")[1]
            
            postfix = "]\n"
            line = line.split("]")[0]
            
            values = line.split(",")
            for value in values:
                value = value.strip()
                newline += (value.rjust(4) + ",")
            
            newline = newline[:-1]
            newline = prefix + newline + postfix
            FINAL_PEDESTAL.write(newline)
        else:
            prefix = 16*" "
            postfix = ""
            if "[" in line:
                prefix = line.split("[")[0] + "["
                line = line.split("[")[1]
                FINAL_PEDESTAL.write(prefix)
            if "]" in line:
                postfix = "]\n"
                line = line.split("]")[0]
                
            values = line.split(",")
            for value in values:
                if value != "\n":   
                    value = value.strip()
                    val_ctr += 1
                    strip_ctr += 1
                    FINAL_PEDESTAL.write(value.rjust(4))
                    if strip_ctr != 512:
                        FINAL_PEDESTAL.write(",")
                    else:
                        strip_ctr = 0
                    if val_ctr == 16:
                        val_ctr = 0
                        FINAL_PEDESTAL.write("\n")
                        FINAL_PEDESTAL.write(16*" ")
                    
            FINAL_PEDESTAL.write(postfix)
    
print("[INFO] Pedestals dumped to YAML file")
    
###--------------------------------------- NOISE---------------------------------------------###

print("[INFO] Gathering info about the noise values...")

noise_dict = {'NoiseValues': {}}
noise_dict['NoiseValues'] = {get_nickname(aChan): {
                        "SectorNoise": 4*[DEFAULT_CMS_NOISE_VALUE],
                        "cmNoise": 4*[DEFAULT_CM_VALUE],
                        "electronsPerADC": DEFAULT_GAIN_VALUE
                       } for aChan in ecs_sectors.values()}


cms_noise_df = pd.read_csv(CMS_NOISE_FILE, header = None, usecols = [0,2], names = ["ChannelID", "CMS_noise"])
full_ECS = [tr_pkl[val] for val in cms_noise_df["ChannelID"]]
cms_noise_df["ECS"] = [val.split(".")[0] for val in full_ECS]
cms_noise_df["Chip"] = [int(val.split(".")[1][4]) for val in full_ECS]
cms_means = cms_noise_df.groupby(["ECS", "Chip"], as_index=False).agg({'CMS_noise':'mean', 'ChannelID':'first'})
for index, row in cms_means.iterrows():
    nickname = get_nickname(row["ChannelID"])
    chip = row["Chip"]
    noise_dict['NoiseValues'][nickname]['SectorNoise'][chip] = round(row["CMS_noise"],6)


cm_df = pd.read_csv(CM_FILE, header = None, usecols = [0,2], names = ["ECS_Chip", "CM"])
for index, row in cm_df.iterrows():
    nickname = get_nickname(ecs_sectors[row["ECS_Chip"][:-2]])
    chip = int(row["ECS_Chip"][-1])
    noise_dict['NoiseValues'][nickname]['cmNoise'][chip] = round(row["CM"],6)


with open(OUTPUT_NOISE, 'w') as outfile:
    yaml.dump(noise_dict, outfile, Dumper=MyDumper, default_flow_style=None, explicit_start=True)
    
print("[INFO] Noise values dumped to YAML file")
    
###------------------------------ READOUT SECTORS AND ZS THRESHOLDS -------------------------------------###
print("[INFO] Gathering info about the ZS thresholds, zs_channels and readout sectors...")

ZS_dict = {'Thresholds': {}}
ZS_dict['Thresholds'] = {get_nickname(aChan): {
                         "ASICThresholds": 4*[DEFAULT_THRESHOLD_VALUE]
                        } for aChan in ecs_sectors.values()}

channels_dict = {'HitLimits': {}}
channels_dict['HitLimits'] = {get_nickname(aChan): {
                              "HitLimit": 4*[DEFAULT_CHANNELS_VALUE]  
                             } for aChan in ecs_sectors.values()}

readout_dict = {'ReadoutSectors': {}}
readout_dict['ReadoutSectors'] = {get_nickname(aChan): {
                        "SALTStatus": 4*[DEFAULT_STATUS_VALUE],
                        "SectorStatus": DEFAULT_STATUS_VALUE,
                        "StripStatus": 512*[DEFAULT_STATUS_VALUE],  
                        "measuredEff": 0.995
                       } for aChan in ecs_sectors.values()}

for INPUT_PATH in REGISTER_DUMPS:
    with open(INPUT_PATH, 'r') as infile:
        for line in infile.readlines():
            if ('n_zs_cfg' not in line) and ('zs_channels_cfg' not in line):
                continue
            else:
                UT_side, UT_sector, UT_chip, UT_register, UT_value = re.split('[:. ]', line)
                
                nickname = get_nickname(ecs_sectors[UT_sector])
                chip = int(UT_chip[-1])
                
                ### Getting ZS threshold from the recipe
                if UT_register == 'n_zs_cfg':
                    ZS_th = int(f"{int(UT_value, 16):08b}"[3:], 2)
                    ZS_dict['Thresholds'][nickname]["ASICThresholds"][chip] = ZS_th
                ### Getting zs_channels from the recipe
                elif UT_register == 'zs_channels_cfg':
                    ZS_channels = int(f"{int(UT_value, 16):08b}"[2:], 2)
                    channels_dict['HitLimits'][nickname]["HitLimit"][chip] = ZS_channels
                else:
                    raise ValueError("Something went wrong with the recipe file! Check your code and the file structure!")
                
### Masking info

with open(MASKING_FILE, 'r') as outfile:
    for line in outfile.readlines():
        raw_ecs = line.split(".masks")[0]
        masks_cfg = line.split(",")[-1].strip("\n")
            
        raw_ecs = raw_ecs.replace(":", ".")
        UT_side, UT_sector, UT_chip = raw_ecs.split(".")
        nickname = get_nickname(ecs_sectors[UT_sector])
        chip = int(UT_chip[-1])
            
        masks_tab = [masks_cfg[i:i+2] for i in range(0, len(masks_cfg), 2)]
        masks_str = ""
        for val in masks_tab:
            masks_str += f"{int(val, 16):08b}"[::-1]
        masks_tab = [int(masks_str[i]) for i in range(len(masks_str))]
        readout_dict['ReadoutSectors'][nickname]['StripStatus'][chip*128:(chip+1)*128] = masks_tab
        if np.any(np.array(masks_tab) == 0):
            readout_dict['ReadoutSectors'][nickname]["SALTStatus"][chip] = 0
        if np.any(np.array(readout_dict['ReadoutSectors'][nickname]["SALTStatus"]) == 0):
                readout_dict['ReadoutSectors'][nickname]["SectorStatus"] = 0

                
with open(OUTPUT_THRESHOLDS, 'w') as outfile:
    yaml.dump(ZS_dict, outfile, Dumper=MyDumper, default_flow_style=None, explicit_start=True)            

print("[INFO] ZS thresholds dumped to YAML file")         

with open(OUTPUT_ZS_CHANNELS, 'w') as outfile:
    yaml.dump(channels_dict, outfile, Dumper=MyDumper, default_flow_style=None, explicit_start=True)            

print("[INFO] ZS channels info dumped to YAML file")     
                
with open(OUTPUT_READOUT, 'w') as outfile:
    yaml.dump(readout_dict, outfile, Dumper=MyDumper, default_flow_style=None, explicit_start=True, width=112)
    
print("[INFO] Readout sectors dumped to YAML file")