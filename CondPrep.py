import os
import subprocess
import yaml
import pickle
import pandas as pd

# PULL_WHOLE_REPO = True
# if PULL_WHOLE_REPO == False:
RUN_NUMBER = 306326 
LOCAL_UT_CONDDB_REPO = "/data1/DATA_SOWA/LocalCondDB/" # Empty directory or existing UT_CONDDB repo
BRANCH = "wojtek_dev"

PLUS_TUNNELLING_NEEDED = True
if PLUS_TUNNELLING_NEEDED:
    PLUS_USER = "ksowa"

########################################### Getting CSV files from UTCondDB #########################################################

CM_dir = os.getcwd()
subprocess.run(f"mkdir -p {LOCAL_UT_CONDDB_REPO}", shell=True)
os.chdir(LOCAL_UT_CONDDB_REPO)

# check if the directory is empty
empty_code = subprocess.run("[ -z \"$( ls -A '.' )\" ]", shell=True).returncode
if empty_code != 0: #if it is not empty
    # check if we are in an exisiting repository 
    exit_code = subprocess.run("git tag > /dev/null 2>&1", shell=True).returncode
    if exit_code != 0:  # if it is not a repo
        raise RuntimeError("LOCAL_UT_CONDDB_REPO is neither an empty directory nor a repository")
    else: #if it is a repo
        git_url = subprocess.run(f"git config --get remote.origin.url", shell=True, capture_output=True).stdout.decode()
        if "lhcb-conddb/UTCOND.git" not in git_url:
            raise RuntimeError("LOCAL_UT_CONDDB_REPO is a wrong repository (it was not cloned from lhcb-conddb/UTCOND.git)")
        
        # in other case we are in a proper repository and the script can continues
        
#################################################### Pulling the whole repo ##################################################################

# Currently disabled

# if PULL_WHOLE_REPO:
#     if (empty_code == 0): #if the dir is empty
#         subprocess.run("git clone https://gitlab.cern.ch/lhcb-conddb/UTCOND.git .", shell=True)
#     subprocess.run("git sparse-checkout disable", shell=True)
#     subprocess.run(f"git checkout {BRANCH}", shell=True)
#     subprocess.run("git pull", shell=True)
    
################################################ Pulling just a specific directory ###########################################################

#else:
if (empty_code == 0): #if the dir is empty
    subprocess.run(f"git clone --filter=blob:none --branch={BRANCH} --no-checkout --depth 1 --sparse https://gitlab.cern.ch/lhcb-conddb/UTCOND.git .", shell=True)
subprocess.run(f"git sparse-checkout add NZS_data/{RUN_NUMBER}/", shell = True)
subprocess.run(f"git checkout {BRANCH} > /dev/null 2>&1", shell=True)

############################################## Download necessary files from plus #################################################################

#  Downloading files with masking information from plus
if PLUS_TUNNELLING_NEEDED:
    while True:
        if subprocess.run(f"scp {PLUS_USER}@plus:$(ssh {PLUS_USER}@plus 'ls -t /group/ut/ECS/RegisterDump/UA/*register_dump* | head -1') {LOCAL_UT_CONDDB_REPO}NZS_data/{RUN_NUMBER}/register_dump_A_side.txt", shell=True).returncode == 0:
            break
    while True:
        if subprocess.run(f"scp {PLUS_USER}@plus:$(ssh {PLUS_USER}@plus 'ls -t /group/ut/ECS/RegisterDump/UC/*register_dump* | head -1') {LOCAL_UT_CONDDB_REPO}NZS_data/{RUN_NUMBER}/register_dump_C_side.txt", shell=True).returncode == 0:
            break
else:
    subprocess.run(f"cp $(ls -t /group/ut/ECS/RegisterDump/UA/*register_dump* | head -1) ./NZS_data/{RUN_NUMBER}/register_dump_A_side.txt", shell=True)
    subprocess.run(f"cp $(ls -t /group/ut/ECS/RegisterDump/UC/*register_dump* | head -1) ./NZS_data/{RUN_NUMBER}/register_dump_C_side.txt", shell=True)

#####################################################################################################################################