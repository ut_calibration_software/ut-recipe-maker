This is a joint repository for 2 pieces of UT software - **the Recipe Maker** and **the Condition Maker**. Necessary information on both of them is provided below.

## Running Recipe Maker

If you want to run the Recipe Maker **from the stack directory** type:

~~~
utils/run-env Vetra python3 ./Vetra/Ut/UTRecipeMaker/RecipeMaker.py
~~~

If you want to run it faster and independently from GAUDI, get rid of all **os.environ** variables in the parameters section and replace them with your path to VETRA directory and then run with:

~~~
python3 ./Vetra/Ut/UTRecipeMaker/RecipeMaker.py
~~~

(works for Python > 3.6)

---

Specify all the parameters values: 

- **INPUT_PATH** - path to the old recipe (json) or txt dump file containing information from all the registers
- **OUTPUT_PATH** - path where the updated recipe will be created (json)
- **VETRA_PEDESTALS_PATH** - path to the .csv file from VETRA with pedestal values
- **VETRA_DISABLED_ASICS_PATH** - path to the .txt file from VETRA with a list of ASICs disabled at the ECS level
- **ECS_TO_CHID_PATH** - path to the file translating ECS name into Channel ID (online_name_to_chanid_map.yaml)
- **CHID_TO_ECS_PATH** - path to the file translating ChannelID into ECS name (translator.pkl)
<!-- - **VETRA_INCOHERENT_NOISE_PATH** - path to the .csv file from VETRA with incoherent noise values
- **VETRA_BAD_CHANNELS_PATH** - path to the .txt file from VETRA with a list of channels masks -->
<!-- **VETRA_ZS_THRESHOLD_COEFF** $:= a_{zs}$ - coefficient used to calculate ZS threshold - the ZS threshold value isset as $a_{zs} \braket{V_{rms}}$ (with $\braket{V_{rms}}$ being the mean incoherent noise RMS value over chip) -->

### How does the Recipe Maker work?

#### Important notice!

Recipe Maker creates the recipe dynamically based on the input file. It allows the user to use only the registers they want and add more of them without any need to change the code. **HOWEVER, if some modules are missing in the input file they will be absent in the output recipe as well !!!** If you want to make sure that all the modules are present in the recipe, use *Recipe_Default_A* and *Recipe_Default_C* as inputs. 

#### Pedestals

The Recipe Maker updates *pedestals_cfg* register values based on the VETRA pedestal file (**VETRA_PEDESTALS_PATH**). The values are updated everywhere except for the disabled ASICs, list of which is provided by the user (**VETRA_DISABLED_ASICS_PATH**)

## Running Condition Maker

In order to obtain YAML condition files, one needs to take a few additional steps before running the `ConditionMaker.py`. 

1) Go to [UTCondDB](https://gitlab.cern.ch/lhcb-conddb/UTCOND/-/tree/wojtek_dev/NZS_data) (branch: wojtek_dev, directory: NZS_data) and check the metadata files to identify the latest NZS run, which fulfils the following conditions:
    - TrimDACs applied
    - HV ON
    - Value of SALT register `nzs_sel = 00`. It means that the NZS data are taken in MASKED mode (after masking, but before pedestal subtraction and MCM subtraction). It ensures that the calibration parameters are properly calculated
2) Run `python3 CondPrep.py` after changing the parameters at the beginning of the file. This scripts clones (pulls) just a specific directory from the UTCondDB repo, corresponding to the run number that you selected. It also downloads other necessary files from the `plus` server. After running this script all `ConditionMaker.py` dependencies should be stored conveniently in one directory.
3) Run `python3 ConditionMaker.py` after changing the parameters at the beginning of the file.

If you have any questions, please let me know (Karol Sowa; karol.mateusz.sowa@cern.ch)