"""
RecipeMaker.py

Created on: December, 2023
Author: Karol Sowa (karol.mateusz.sowa@cern.ch)

"""

from RecipeHelper import *

#################################################### PARAMETERS ##########################################################

"""
Newest recipes can be found at:
/group/ut/ECS/Recipes/Hybrids/Dump/UA/DEFAULT/Configure
/group/ut/ECS/Recipes/Hybrids/Dump/UC/DEFAULT/Configure

Newest register dumps can be found at:
/group/ut/ECS/RegisterDump/UA
/group/ut/ECS/RegisterDump/UC

"""
# INPUT_PATH =  os.environ["ENV_PROJECT_SOURCE_DIR"] + "/Ut/UTRecipeMaker/Recipe_Default_C.json"
# OUTPUT_PATH =  os.environ["ENV_PROJECT_SOURCE_DIR"] + "/recipe_C_0000282213.json"
INPUT_PATH =  "/data1/DATA_SOWA/SALT_reg_studies/Oct31/UA/register_dump_20241031T163453.txt"
OUTPUT_PATH = "./Recipe_Updated_A.json"

# VETRA_PEDESTALS_PATH =  os.environ["ENV_PROJECT_SOURCE_DIR"] + "/pedestals_0000282213.csv"
VETRA_PEDESTALS_PATH =  "/data1/DATA_SOWA/UTCondDB/NZS_data/309395/pedestals.csv"

# VETRA_DISABLED_ASICS_PATH =  os.environ["ENV_PROJECT_SOURCE_DIR"] + "/disabled_asics_0000282213.txt"
VETRA_DISABLED_ASICS_PATH = "./disabled_0000309697.csv"

# ECS_TO_CHID_PATH = os.environ["ENV_PROJECT_SOURCE_DIR"] + "/Ut/UTRecipeMaker/online_name_to_chanid_map.yaml"
# CHID_TO_ECS_PATH = os.environ["ENV_PROJECT_SOURCE_DIR"] + "/Ut/UTRecipeMaker/translator.pkl"
ECS_TO_CHID_PATH = "./online_name_to_chanid_map.yaml"
CHID_TO_ECS_PATH = "./translator.pkl"

######################################## Loading the list of disabled ASICS ###############################################

DISABLED_ASICS = np.loadtxt(VETRA_DISABLED_ASICS_PATH, dtype='str')

############################################# Loading translator files ####################################################

with open(ECS_TO_CHID_PATH, 'r') as file:
    try:
        ecs_sectors = yaml.safe_load(file)
    except yaml.YAMLError as exc:
        print(exc)
        
with open(CHID_TO_ECS_PATH, 'rb') as handle:
    translator = pickle.load(handle)
    
############################################# Reading old recipe / txt file ###################################################

INPUT_FMT = INPUT_PATH.split(".")[-1]
OUTPUT_FMT = OUTPUT_PATH.split(".")[-1]

UT_dict = {
           'UADAQFEE': {},
           'UCDAQFEE': {}
           }

if (INPUT_FMT == 'txt'):
    
    f = open(INPUT_PATH, 'r')
    lines = f.read().splitlines()

    i = 0
    nlines = len(lines)
    while i < nlines:
        UT_side, UT_sector, UT_chip, UT_register, UT_value = re.split('[:. ]', lines[i])
        
        UT_dict[UT_side][UT_sector] = {}
        UT_dict[UT_side][UT_sector][UT_chip] = {}
        UT_dict[UT_side][UT_sector][UT_chip][UT_register] = UT_value
                 
        for new_sector_index in range(i+1, nlines):
            new_UT_sector = re.split('[:. ]', lines[new_sector_index])[1]
            if new_UT_sector != UT_sector: break
        
        if new_sector_index == nlines - 1:
            new_sector_index += 1
        
        
        reg_index = i + 1

        while reg_index < new_sector_index:
        
            for new_chip_index in range(reg_index, new_sector_index):
                new_UT_chip = re.split('[:. ]', lines[new_chip_index])[2]
                if new_UT_chip != UT_chip:
                    UT_dict[UT_side][UT_sector][new_UT_chip] = {}
                    break
            if(new_chip_index == (new_sector_index - 1)): new_chip_index += 1
                
            
            while reg_index < new_chip_index:
                UT_chip, UT_register, UT_value = re.split('[:. ]', lines[reg_index])[2:]   #UT_chip added to make it work for one-register recipe
                
                if UT_register == 'pedestals_cfg' or UT_register == 'baselines_cfg':
                    UT_dict[UT_side][UT_sector][UT_chip][UT_register] = [UT_value[i:i+2] for i in range(0, len(UT_value), 2)]
                else:
                    UT_dict[UT_side][UT_sector][UT_chip][UT_register] = UT_value
                reg_index += 1
            
            UT_chip = new_UT_chip
            
   
        i = reg_index
        
    if(len(UT_dict['UADAQFEE'])) == 0: del UT_dict['UADAQFEE']
    if(len(UT_dict['UCDAQFEE'])) == 0: del UT_dict['UCDAQFEE']
    
    f.close()
        
elif(INPUT_FMT == 'json'):
    f = open(INPUT_PATH)
    UT_dict = json.load(f)
    f.close()
    
########################################### UPDATING PEDESTAL INFO FROM VETRA ######################################################

data = pd.read_csv(VETRA_PEDESTALS_PATH, header = None)

for i in range(len(data)):
    ecs_str = translator[data.iloc[i,0]]
    UT_sector, UT_chip, CHANNEL = ecs_str.split('.')
        
    CHANNEL = int(CHANNEL[2:])
    if UT_sector[6] == 'A': UT_side = 'UADAQFEE'
    elif UT_sector[6] == 'C': UT_side = 'UCDAQFEE' 
    
    if f"{UT_sector}.{UT_chip}" in DISABLED_ASICS:
        continue
    elif UT_side not in UT_dict.keys():
        continue
    elif UT_sector not in UT_dict[UT_side].keys():
        continue
    elif UT_chip not in UT_dict[UT_side][UT_sector].keys():
        continue
    else:
        dec_value = int(round(data.iloc[i,1]))
        
        if 'E' not in UT_sector and 'W' not in UT_sector:
            if dec_value == -32:
                dec_value = -31
            dec_value = -dec_value
        twoc_value = int_to_twoc(dec_value)
        hex_value = str(f"{int(twoc_value,2):02x}")
        UT_dict[UT_side][UT_sector][UT_chip]['pedestals_cfg'][CHANNEL] = hex_value
    
############################################## Writing updated recipe to file ##############################################

json_object = json.dumps(UT_dict, indent = 2)

if (OUTPUT_FMT == 'json'):
    with open(OUTPUT_PATH, "w") as outfile:
        outfile.write(json_object)
    print(f"Config file written to: {OUTPUT_PATH}")
        
else:
    raise Exception(f"Unsupported output recipe format: {OUTPUT_FMT}")


##################################################################################################################################
