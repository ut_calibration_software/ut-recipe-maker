import os
import pickle

########################################## Parameters ###################################################

INPUT_PATH = "/data4/krupaw/stack/Vetra/Ut/NewBadChanels.csv"
OUTPUT_PATH = "/data4/krupaw/stack/Vetra/Ut/NewBadChanels_Translated.csv"

########################################### Translating part ###############################################

f = open(INPUT_PATH, 'r')
lines = f.read().splitlines()

with open(os.environ["ENV_PROJECT_SOURCE_DIR"] + '/Ut/UTRecipeMaker/translator.pkl', 'rb') as handle:
    translator = pickle.load(handle)

new_lines = [translator[int(line)] + '\n' for line in lines]

out_f = open(OUTPUT_PATH, 'w') 

for new_line in new_lines:
    out_f.write(new_line)


f.close()
out_f.close()  