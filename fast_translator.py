# utils/run-env Vetra python3 ./Vetra/Ut/fast_translator.py

import yaml
import numpy as np
import os

INFILE_NAME = os.environ["ENV_PROJECT_SOURCE_DIR"] + "/Ut/UTRecipeMaker/badCh_20_f_ci.txt"
OUTFILE_NAME = os.environ["ENV_PROJECT_SOURCE_DIR"] + "/Ut/UTRecipeMaker.outfile.csv"

with open(os.environ["ENV_PROJECT_SOURCE_DIR"] + "/Ut/UTRecipeMaker/online_name_to_chanid_map.yaml", 'r') as file:
    try:
        translator_dict = yaml.safe_load(file)
    except yaml.YAMLError as exc:
        print(exc)
        
data = open(INFILE_NAME, 'r')
output = open(OUTFILE_NAME, 'w')
line_ctr = 0
for line in data:
    ECS = line.split("[")[0][4:-3]
    base_chid = translator_dict[ECS]
    Chip = int(line.split("[")[0][-2])
    ch_tab = line.split(' [')[1][:-2].replace("'", "").split(",")
    ascending = ""
    for module in ch_tab:
        ascending += f"{eval(module):08b}"[::-1]
    for i in range(128):
        #line_ctr += 1
        channel_no = Chip*128 + i
        CHID = base_chid + channel_no
        flag = ascending[i] 
        output.write(f"{CHID}, {flag} \n")    
