import yaml
import json
import os
import numpy as np
import pickle
import re


############################################## Parameters ##################################################
INPUT_PATH = "/data1/DATA_SOWA/SALT_reg_studies/temp.json"
OUTPUT_PATH = "/data1/DATA_SOWA/SALT_reg_studies/temp.txt"
############################################################################################################
INPUT_FMT = INPUT_PATH.split(".")[-1]
OUTPUT_FMT = OUTPUT_PATH.split(".")[-1]

UT_dict = {
           'UADAQFEE': {},
           'UCDAQFEE': {}
           }

if (INPUT_FMT == 'txt' and OUTPUT_FMT == 'json'):
    
    f = open(INPUT_PATH, 'r')
    lines = f.read().splitlines()

    i = 0
    nlines = len(lines)
    while i < nlines:
        UT_side, UT_hybrid, UT_chip, UT_register, UT_value = re.split(':|\.| ', lines[i])
        
        
        UT_dict[UT_side][UT_hybrid] = {}
        UT_dict[UT_side][UT_hybrid][UT_chip] = {}
        UT_dict[UT_side][UT_hybrid][UT_chip][UT_register] = UT_value
        
        
            
        for new_hybrid_index in range(i+1, nlines):
            new_UT_hybrid = re.split(':|\.| ', lines[new_hybrid_index])[1]
            if new_UT_hybrid != UT_hybrid: break
        
        if new_hybrid_index == nlines - 1:
            new_hybrid_index += 1
        
        
        reg_index = i + 1

        while reg_index < new_hybrid_index:
        
            for new_chip_index in range(reg_index, new_hybrid_index):
                new_UT_chip = re.split(':|\.| ', lines[new_chip_index])[2]
                if new_UT_chip != UT_chip:
                    UT_dict[UT_side][UT_hybrid][new_UT_chip] = {}
                    break
            if(new_chip_index == (new_hybrid_index - 1)): new_chip_index += 1
                
            
            while reg_index < new_chip_index:
                UT_register, UT_value = re.split(':|\.| ', lines[reg_index])[3:]
                
                if UT_register == 'pedestals_cfg' or UT_register == 'baselines_cfg':
                    UT_dict[UT_side][UT_hybrid][UT_chip][UT_register] = [UT_value[i:i+2] for i in range(0, len(UT_value), 2)]
                else:
                    UT_dict[UT_side][UT_hybrid][UT_chip][UT_register] = UT_value
                reg_index += 1
            
            UT_chip = new_UT_chip
            
   
        i = reg_index
        
    if(len(UT_dict['UADAQFEE'])) == 0: del UT_dict['UADAQFEE']
    if(len(UT_dict['UCDAQFEE'])) == 0: del UT_dict['UCDAQFEE']

    json_object = json.dumps(UT_dict, indent = 2)

    with open(OUTPUT_PATH, "w") as outfile:
        outfile.write(json_object)
    
    f.close()
        
elif(INPUT_FMT == 'json' and OUTPUT_FMT == 'txt'):
    f = open(INPUT_PATH)
    UT_dict = json.load(f)
    
    out_f = open(OUTPUT_PATH, 'w')
    
    for UT_side in UT_dict.keys():
        for UT_hybrid in UT_dict[UT_side].keys():
            for UT_chip in UT_dict[UT_side][UT_hybrid].keys():
                for UT_register, UT_value in UT_dict[UT_side][UT_hybrid][UT_chip].items():
                    if UT_register == 'pedestals_cfg' or UT_register == 'baselines_cfg':
                        UT_value = ''.join(UT_value)
                        
                    line = UT_side + ":" + UT_hybrid + "." + UT_chip + "." + UT_register + " " + UT_value + "\n"
                    out_f.write(line)
                
    f.close()
    out_f.close()
else:
    raise Exception("Wrong file formats for json<->txt converter!")
    